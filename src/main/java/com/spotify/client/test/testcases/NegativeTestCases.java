package com.spotify.client.test.testcases;

import org.sikuli.script.FindFailed;
import org.testng.annotations.Test;

import com.spotify.client.test.framework.ImageNames;

/**
 * This class contains negative tests that verifies expected failure functional scenarios in the Spotify client
 * @author Per-Erik Björkstad
 *
 */
public class NegativeTestCases extends TestCasesBase {


	/**
	 * This test verifies that a failure message appears when incorrect log in credentials are given.
	 * @throws FindFailed - Thrown if the log in screen could not be found.
	 */
	@Test
	public void logInFailedTest() throws FindFailed {

		client.logOut();
		client.logIn("nonExistingUser", "SomeWierdPassword");

		client.getScreen().wait(ImageNames.getFailedLoginIcon(), 5);
	}

}
