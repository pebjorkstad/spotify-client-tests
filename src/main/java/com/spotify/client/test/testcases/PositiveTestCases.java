package com.spotify.client.test.testcases;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Match;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.spotify.client.test.framework.ConfigFile;
import com.spotify.client.test.framework.ImageNames;

/**
 * This class contains a set of tests to verify basic functionality in the Spotify client
 * @author Per-Erik Björkstad
 *
 */
public class PositiveTestCases extends TestCasesBase  {


	/**
	 * This method is run before all test methods in this class.
	 * It logs in to the Spotify client using valid credentials and verifies the log in was successful.
	 * @throws FindFailed - Thrown if a match on the screen for some steps could not be found, i.e. a test step failed.
	 */
	@BeforeClass
	public void beforeClass() throws FindFailed {

		client.activateWindow();
		client.logOut();
		client.logIn(ConfigFile.getUser(), ConfigFile.getPassword());

		client.getScreen().wait(ImageNames.getSearchField(), 5);
	}


	/**
	 * This test searches for a song, starts playing it and verifies that it is playing.
	 * @throws FindFailed - Thrown if a match on the screen for some steps could not be found, i.e. a test step failed.
	 */
	@Test
	public void playOneSongTest() throws FindFailed {

		client.search("Coldplay - Clocks");

		Match result = client.getScreen().wait(ImageNames.getResultsClocks(), 5);
		result.belowAt().doubleClick();

		client.getScreen().wait(ImageNames.getClocksIsPlayingInSearchResults(), 5).highlight(1);
		client.getScreen().wait(ImageNames.getSomethingIsPlaying(), 5).highlight(1);
	}


	/**
	 * Tests 3 different kind of searches and verifies that some results are shown.	
	 * @throws FindFailed - Thrown if a match on the screen for some steps could not be found, i.e. a test step failed.
	 */
	@Test
	public void searchTest() throws FindFailed {

		client.search("Metallica");
		client.getScreen().wait(ImageNames.getResultsMetallica(), 5);
		client.getScreen().find(ImageNames.getVerifyResultsMetallica()).highlight(1);

		client.search("Metallica Enter Sandman");
		client.getScreen().wait(ImageNames.getResultsEnterSandman(), 5);
		client.getScreen().find(ImageNames.getVerifyResultsEnterSandman()).highlight(1);

		client.search("Gyllene Tider - Sommartider");
		client.getScreen().wait(ImageNames.getResultsSommartider(), 5);
		client.getScreen().find(ImageNames.getVerifyResultsSommartider()).highlight(1);
	}


	/**
	 * Creates a playlist, verifies that it has been created and deletes it afterwards
	 * @throws FindFailed - Thrown if a match on the screen for some steps could not be found, i.e. a test step failed.
	 */
	@Test
	public void createPlaylistTest() throws FindFailed {

		client.getScreen().wait(ImageNames.getPlaylistNewButton(), 5).click();
		client.getScreen().wait(ImageNames.getCreatePlaylistWindow(), 5);

		Match playlistNameField = client.getScreen().wait(ImageNames.getPlaylistNameField(), 5);
		client.clearTextField(playlistNameField);
		playlistNameField.type("Christmas");
		playlistNameField.type(Key.ENTER);

		Match playlistChristmas = client.getScreen().wait(ImageNames.getPlaylistChristmas(), 5);
		playlistChristmas.highlight(1);

		playlistChristmas.rightClick();
		client.getScreen().click(ImageNames.getPlaylistDeleteButton());
	}

}
