package com.spotify.client.test.testcases;

import java.io.File;
import java.net.URL;

import org.sikuli.basics.Debug;
import org.sikuli.script.FindFailed;
import org.sikuli.script.ImagePath;
import org.testng.annotations.BeforeMethod;

import com.spotify.client.test.framework.ImageNames;
import com.spotify.client.test.framework.SpotifyClient;

/**
 * This class contains common functionality for test case classes
 * @author Per-Erik Björkstad
 *
 */
public class TestCasesBase {

	SpotifyClient client;

	/**
	 * Constructor. Sets up the image search path and initiates class variables.
	 */
	public TestCasesBase() {

		Debug.on(3);

		ClassLoader classLoader = getClass().getClassLoader();
		URL spotifyLogoUrl = classLoader.getResource("images/" + ImageNames.getSpotifyLogo());
		String imagesLocation = new File(spotifyLogoUrl.getPath()).getParentFile().getAbsolutePath();
		ImagePath.setBundlePath(imagesLocation);

		client = new SpotifyClient();
	}

	/**
	 * Runs before each test. This method sets the Spotify window as the active window and does cleanup between tests.
	 * @throws FindFailed - Thrown if the Spotify client window could not be found.
	 */
	@BeforeMethod
	public void beforeMethod() throws FindFailed {
		client.activateWindow();
	}

}
