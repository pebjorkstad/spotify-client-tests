package com.spotify.client.test.framework;

/**
 * Utility class used to retrieve information about the system
 * @author Per-Erik Björkstad
 *
 */
public class SystemUtils {

	private static final String OS = System.getProperty("os.name").toLowerCase();

	/**
	 * Checks if the operating system used is Mac
	 * @return True if the OS is OSX on Mac, False otherwise.
	 */
	public static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	/**
	 * Checks if the operating system used is Windows
	 * @return True if the OS is Windows, False otherwise.
	 */
	public static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}
}
