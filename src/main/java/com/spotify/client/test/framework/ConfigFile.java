package com.spotify.client.test.framework;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

/**
 * This class provides methods to retrieve property values from the properties file
 * @author Per-Erik Björkstad
 *
 */
public class ConfigFile {

	private static final Properties config = new Properties();

	/**
	 *  Load the configuration file when this class is loaded by the classloader.
	 */
	static {
		init();
	}

	/**
	 * Reads the properties file from disk and parses it.
	 */
	protected static void init() {

		String configurationFilePath = System.getProperty("configurationFile");

		if (configurationFilePath == null) {
			ClassLoader classLoader = ConfigFile.class.getClassLoader();
			URL propertiesFileUrl = classLoader.getResource("test.properties");
			configurationFilePath = propertiesFileUrl.getPath();
		}

		try {

			InputStream stream = new FileInputStream(configurationFilePath);
			config.load(stream);
			stream.close();

		} catch (FileNotFoundException e) {
			throw new RuntimeException("Configuration file '" + configurationFilePath + "' not found.", e);
		} catch (IOException e) {
			throw new RuntimeException("Loading configuration file '" + configurationFilePath + "' failed", e);
		}
	}

	/**
	 * Retrieves the username
	 * @return
	 */
	public static String getUser() {
		return config.getProperty("user");
	}

	/**
	 * Retrieves the password for the user
	 * @return
	 */
	public static String getPassword() {
		return config.getProperty("password");
	}

}
