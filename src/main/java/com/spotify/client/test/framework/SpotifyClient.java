package com.spotify.client.test.framework;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.KeyModifier;
import org.sikuli.script.Match;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;


/**
 * Spotify Client window helper class.
 * Use instances of this class to interact with a Spotify client window.
 * @author Per-Erik Björkstad
 */
public class SpotifyClient {

	Screen screen;

	/**
	 * Constructor
	 */
	public SpotifyClient() {
		screen = new Screen();
	}

	/**
	 * Set focus on the Spotify client window
	 * @throws FindFailed - Thrown if the Spotify client window could not be found on screen
	 */
	private void focus() throws FindFailed {

		Match match = screen.exists(ImageNames.getSearchField(), 1);

		if (match == null) {
			match = screen.exists(ImageNames.getSpotifyLogo(), 1);
		}

		if (match == null) {
			throw new FindFailed("Could not identify the Spotify client window.");
		}

		match.click();
	}

	/**
	 * Close the application
	 */
	public void close() {

		if(SystemUtils.isWindows()) {
			getScreen().type(Key.F4, KeyModifier.ALT);
		} else if (SystemUtils.isMac()) {
			getScreen().type("q", KeyModifier.CMD);
		}
	}

	/**
	 * Returns the object representation the current screen 
	 * @return The current screen object
	 */
	public Screen getScreen() {
		return screen;
	}

	/**
	 * Finds the Spoticy client window, sets it to the active window and deactivates any active popup dialogs.
	 * @throws FindFailed - Thrown if the Spotify client window could not be found on screen
	 */
	public void activateWindow() throws FindFailed {

		focus();

		// Key shortcuts does not work when a dropdown is active. E.g. if the search field is marked and provides
		// a drop-down of auto-complete search suggestions, one can not use the key shortcut to logout. Therefore we
		// make sure all such drop-downs are closed by pressing the ESC button twice.
		// This could be considered a bug in the Spotify client.
		getScreen().type(Key.ESC);
		getScreen().type(Key.ESC);
	}

	/**
	 * Clears a text field
	 * @param field A text field to be cleared
	 */
	public void clearTextField(Region field) {

		if( SystemUtils.isWindows() ) {
			field.type("a", KeyModifier.CTRL);
		} else if (SystemUtils.isMac()) {
			field.type("a", KeyModifier.CMD);
		}

		field.type(Key.DELETE);
	}

	/**
	 * Logs out the user.
	 */
	public void logOut() throws FindFailed {

		activateWindow();

		if( SystemUtils.isWindows() ) {
			getScreen().type("w", KeyModifier.CTRL + KeyModifier.SHIFT);
		} else if (SystemUtils.isMac()) {
			getScreen().type("w", KeyModifier.CMD + KeyModifier.SHIFT);
		}

		getScreen().wait(ImageNames.getSpotifyLogo(), 5);
	}

	/**
	 * Logs in to the Spotify client.
	 * Requires that the Spotify client is started and is showing the log in screen.
	 * @param user - The username to use
	 * @param password - The password for the username
	 * @throws FindFailed - Thrown if the login screen can't be found.
	 */
	public void logIn(String user, String password) throws FindFailed {

		activateWindow();

		Match loginField = getScreen().wait(ImageNames.getLoginField());
		loginField.getCenter().click();

		clearTextField(loginField);

		loginField.type(user);
		loginField.type(Key.TAB);
		loginField.type(password);
		loginField.type(Key.ENTER);
	}

	/**
	 * Searches Spotify for a given string.
	 * Requires that a user is logged in and that the search field is visible. 
	 * @param searchString - The string to search for
	 * @throws FindFailed - Thrown if the search field could not be found on screen.
	 */
	public void search(String searchString) throws FindFailed {

		getScreen().type(Key.ESC);
		getScreen().type(Key.ESC);

		Match searchField = getScreen().wait(ImageNames.getSearchField(), 5);
		searchField.click();

		clearTextField(searchField);
		searchField.type(Key.DELETE);

		searchField.type(searchString);
		searchField.type(Key.ENTER);
	}

}
