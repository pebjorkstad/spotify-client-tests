package com.spotify.client.test.framework;

/**
 * This is a getter class for all the filenames of all the images used in this framework and in the tests
 * @author Per-Erik Björkstad
 *
 */
public class ImageNames {

	public static String getSpotifyLogo() {
		return "SpotifyLogo.png";
	}

	public static String getSearchField() {
		return "SearchField.png";
	}

	public static String getLoginField() {
		return "LoginField.png";
	}

	public static String getSomethingIsPlaying() {

		if (SystemUtils.isMac()) {
			return "SomethingIsPlaying-Mac.png";
		} else {
			return "SomethingIsPlaying.png";
		}
	}

	public static String getPlaylistNewButton() {

		if(SystemUtils.isMac()) {
			return "PlaylistNewButton-Mac.png";
		} else {
			return "PlaylistNewButton.png";
		}
	}

	public static String getResultsClocks() {
		return "ResultsClocks.png";
	}

	public static String getClocksIsPlayingInSearchResults() {
		return "ClocksIsPlayingInSearchResults.png";
	}

	public static String getResultsMetallica() {
		return "ResultsMetallica.png";
	}

	public static String getVerifyResultsMetallica() {
		return "VerifyResultsMetallica.png";
	}

	public static String getResultsEnterSandman() {
		return "ResultsEnterSandman.png";
	}

	public static String getVerifyResultsEnterSandman() {
		return "VerifyResultsEnterSandman.png";
	}

	public static String getResultsSommartider() {
		return "ResultsSommartider.png";
	}

	public static String getVerifyResultsSommartider() {
		return "VerifyResultsSommartider.png";
	}

	public static String getCreatePlaylistWindow() {
		return "CreatePlaylistWindow.png";
	}

	public static String getPlaylistNameField() {
		return "PlaylistNameField.png";
	}

	public static String getPlaylistChristmas() {
		return "PlaylistChristmas.png";
	}

	public static String getPlaylistDeleteButton() {
		return "PlaylistDeleteButton.png";
	}

	public static String getFailedLoginIcon() {
		return "FailedLoginIcon.png";
	}
	
}
