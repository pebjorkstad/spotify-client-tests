This project contains basic GUI functional tests for the Spotify desktop client


Prerequisites:
* The Spotify client must be installed, running and visible on the screen.
* The Spotify backend must be running and accessible by the client.
* The Spotify client language should be set to English.
  - Other language settings might work, but the stability of these tests
    has not been verified with any other language settings.


The tests have been verified with these versions of the Spotify client:
* Windows 10.0 (Build 10240): 1.0.41.375.g040056ca
* OS X El Capitan (10.11.5): 1.0.34.146.g28f9eda2


Running tests from command line:
* In the root directory run the maven goal 'test': mvn test
* This will fetch all necessary dependencies, compile the source and run the tests.


Running test cases from Eclipse:
* Import the code as an Existing Maven Project and point to the pom.xml in the root directory.
* You will need to install the TestNG Eclipse plugin. Instructions here: http://testng.org/doc/eclipse.html#eclipse-installation
* All tests are defined in classes in the package com.spotify.client.test.testcases.
* To run a test right-click a method annotated @Test -> Run As -> TestNG Test
* Running tests within Eclipse is further described here: http://testng.org/doc/eclipse.html#eclipse-create-method


Configuring the tests:
  The tests can be configured to use a different user than the default user 'eandersson9'. To use your own user,
  create a copy of src/main/resources/test.properties, modify the user credentials according to your needs,
  then run the tests using your own properties file by setting the environment variable 'configurationFile'.
  It is recommended to set this on the command line with the -D option:
    mvn test -DconfigurationFile=<full/path/to/config.properties>


Troubleshooting:
* If you are behind a firewall you might need to configure maven to use a proxy:
  https://maven.apache.org/guides/mini/guide-proxies.html
* The projects pom.xml refers to the sikulixapi.jar in the Maven Central repository. While this jar file should work,
  it seems to have some limitations on Windows 10. If you encounter problems on Windows 10, please download and install
  the SikuliX API manually. Instructions here: http://www.sikulix.com/quickstart/
  The local sikulixapi.jar can be referred to by the tests by providing the flag -Dsikulixapi=<full/path/to/jar>
  Like so:
  - mvn test -Dsikulixapi=<full/path/to/jar>


Future improvements:

1) To simplify the usage of these tests and the test framework, the framework could launch the Spotify Client executable and
   handle the lifetime of the client process, i.e. shut it down when the tests are done. This could simplify the code in
   SpotifyClient.java, e.g. in the focus() method. The executable to launch could be done by launching e.g. Spotify.exe from
   the PATH or it could be set in a property containing the full path to the executable.
   The class App (org.sikuli.script.App) seems suitable for this purpose. Usage of it was investigated during the development of
   this test framework, but it was found too unstable and too unreliable in its current state to control the active window on Windows 10.d

2) SikuliX has a OCR feature for text recognition. This feature could be used to limit the necessary changes in the tests
   when the GUI changes. This feature is however only in experimental release as of SikuliX 1.1.0, therefore it was not
   used here. Once there is a stable release of this OCR feature, it could be applied on e.g. search result verifications.